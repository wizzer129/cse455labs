package com.example.alex.myapplication;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    EditText editText1;
    TextView textView;
    String  jsonString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText1 = (EditText) findViewById(R.id.editText);
                jsonString = editText1.getText().toString();
                new sendPostRequest().execute("http://date.jsontest.com");
            }
        });
    }

    public class sendPostRequest extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            String data = "";
            String rVal = "";
            String line;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                int code = connection.getResponseCode();
                if (code != -1) {
                    InputStream input = new BufferedInputStream(connection.getInputStream());
                    if (input != null) {
                        BufferedReader bReader = new BufferedReader(new InputStreamReader(input));
                        while ((line = bReader.readLine()) != null) {
                            data += line;
                        }
                        input.close();
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                connection.disconnect();
            }

            try {
                JSONObject jsonObject = new JSONObject(data);
                rVal = jsonObject.getString(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return rVal;
        }
        @Override
        protected void onPostExecute(String result){
            textView = (TextView) findViewById(R.id.textView2);
            textView.setText(result);
        }

    }
}